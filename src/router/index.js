import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Details from '@/components/RecipeDetails';
import AddNewRecipe from '@/components/AddNewRecipe'

Vue.use(VueRouter);

const routes = [{
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/recipeDetails",
        name: "recipeDetails",
        component: Details
    },
    {
        path: '/addNewRecipe',
        name: 'addNewRecipe',
        component: AddNewRecipe
    },
    {
        path: "/about",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: function() {
            return import ( /* webpackChunkName: "about" */ "../views/About.vue");
        }
    }
];

const router = new VueRouter({
    routes
});

export default router;