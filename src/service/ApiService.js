import Api from '../service/Api';

//const accessToken = '4951cdd1649d7a05a97c35116e334f6e99bce013'

export default {
    //'?access_token='+accessToken
    getOrgInfo(companyName) {
        return Api().get('/orgs/' + companyName)
    },
    updateRecipeInfo(recipeId, name, desc, listOfItems) {
        return Api().patch('/api/update/recipe', {
            recipeId: recipeId,
            recipeName: name,
            recipeDescription: desc,
            recipeIngredients: listOfItems
        })
    },


    getAllRecipes() {
        return Api().get('/api/get/all/recipes')
    },
    getAllIngrediants() {
        return Api().get('/api/get/all/ingredients')
    },

    addNewRecie(name, desc, listOfItems) {
        return Api().post('/api/add/new/recipe', {
            recipeName: name,
            recipeDescription: desc,
            ingredientsItem: listOfItems
        })
    },
    getREcipeById(id) {
        return Api().get('/api/get/recipe/by/id/' + id)
    },

    deleteRow(id) {
        return Api().delete('/api/delete/recipe/' + id)
    }
}